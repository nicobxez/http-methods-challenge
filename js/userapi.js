// global variables
const submit = document.querySelector('#submit');
const body = document.querySelector('#body');
const query = document.querySelector('#query');
const params = document.querySelector('#params');
const message = document.querySelector('#message');
const userData = document.querySelector('#userData');

// functions
function postLogin() {
    const user = document.getElementsByName('user')[0].value;
    const pass = document.getElementsByName('pass')[0].value;
    const data = {"user":user, "password":pass};

    fetch('http://192.168.11.31:3000/login', {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify(data)
    })
    .then(res => res.json())
    .then(res => {
        localStorage.setItem('token', res.token);
        document.querySelector('#user').style.display = 'none';
        document.querySelector('#pass').style.display = 'none';
        document.querySelector('#submit').style.display = 'none';
        message.innerHTML=`<h6 class="text-warning">Has iniciado sesion correctamente</h6>`;
        userData.innerHTML=`<input id="name" name="name" type="text" placeholder="Name">
                        <input id="surname" name="surname" type="text" placeholder="Surname">`;
        document.querySelector('#body').style.display = 'inline';
        document.querySelector('#query').style.display = 'inline';
        document.querySelector('#params').style.display = 'inline';
    })
    .catch(err => {
        message.innerHTML=`<h6 class="text-danger">Hubo un error al iniciar sesion</h6>`;
    })
}

function getQuery() {
    const token = localStorage.getItem('token');
    const name = document.querySelector('#name').value;
    const surname = document.querySelector('#surname').value;

    fetch(`http://192.168.11.31:3000/test/query?name=${name}&surname=${surname}`, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': token
        }
    })
    .then(res => res.json())
    .then(res => {
        message.innerHTML=`<h6 class="text-warning">${res.message}</h6>`;
    })
    .catch(err => {
        message.innerHTML=`<h6 class="text-danger">Ingresa un Nombre y Apellido</h6>`;
    })
}

function getParams() {
    const token = localStorage.getItem('token');
    const name = document.querySelector('#name').value;
    const surname = document.querySelector('#surname').value;

    fetch(`http://192.168.11.31:3000/test/params/${name}/${surname}`, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': token
        }
    })
    .then(res => res.json())
    .then(res => {
        message.innerHTML=`<h6 class="text-warning">${res.message}</h6>`;
    })
    .catch(err => {
        message.innerHTML=`<h6 class="text-danger">Ingresa un Nombre y Apellido</h6>`;
    })
}

function postBody() {
    const token = localStorage.getItem('token');
    const name = document.querySelector('#name').value;
    const surname = document.querySelector('#surname').value;
    const profile = { "name": name, "surname": surname };

    fetch('http://192.168.11.31:3000/test/body', {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': token
        },
        method: "POST",
        body: JSON.stringify(profile)
    })
    .then(res => res.json())
    .then(res => {
        message.innerHTML=`<h6 class="text-warning">${res.message}</h6>`;
    })
    .catch(err => {
        message.innerHTML=`<h6 class="text-danger">Ingresa un Nombre y Apellido</h6>`;
    })
    
}

// events
submit.addEventListener("click", function (e){
    e.preventDefault();
    postLogin();
})

body.addEventListener("click", function (e){
    e.preventDefault();
    postBody();
})

query.addEventListener("click", function (e){
    e.preventDefault();
    getQuery();
})

params.addEventListener("click", function (e){
    e.preventDefault();
    getParams();
})